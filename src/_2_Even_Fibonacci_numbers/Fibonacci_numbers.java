package _2_Even_Fibonacci_numbers;

/**
 * https://projecteuler.net/problem=2
 * @author JenChieh
 */
public class Fibonacci_numbers {
    
    private final static int BELOW = 400000000;
    
    public final static void main(final String[] args) {
        int prevNum = 0;  // start with zero!
        
        int totalSum = 0;
        int totalEvenVal = 0;
        
        for (
                int num = 1; 
                num < BELOW;
                ) {
            int temp = num + prevNum;
            prevNum = num;
            num = temp;
            
            // add up only even numbers.
            if ((num % 2) == 0)
                totalEvenVal += num;
            
            totalSum += num;
            
            System.out.println("Fibonacci number is " + num);
        }
        
        System.out.println("Total sum of all number: " + totalSum);
        System.out.println("Total sum of all even number: " + totalEvenVal);
    }
    
}
