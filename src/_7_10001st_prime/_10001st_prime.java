package _7_10001st_prime;

/**
 * https://projecteuler.net/problem=7
 * @author JenChieh
 */
public class _10001st_prime {
    
    /* This occurs call-stack overflow... but I got the idea. :P */
    //private final static long N_TH_PRIME_NUM = 10001L;
    private final static long N_TH_PRIME_NUM = 6L;
    
    public static void main(String[] args) {
        System.out.println(
                "The 10 001st prime number is " 
        + findNthPrimeNumber(N_TH_PRIME_NUM));
    }
    
    
    /**
     * Find out the n-th prime number.
     *
     * @param inIndex : n-th value.
     * @return n-th prime number value.
     */
    private static long findNthPrimeNumber(long nth) {
        return findNthPrimeNumber(nth, 0, 0);
    }
    
    /**
     * Find out the n-th prime number.
     *
     * @param nth : n-th value.
     * @param num : counting.
     * @return n-th prime number value.
     */
    private static long findNthPrimeNumber(long nth, long num, long primeCount) {
        ++num;
        
        if (isPrime(num))
            ++primeCount;
        
        if (primeCount == nth)
            return num;
        
        return findNthPrimeNumber(nth, num, primeCount);
    }
    
    /**
     * Check if the number prime number.
     * 
     * @param num : number to check.
     * @return true : is prime number, false : vice versa.
     */
    private static boolean isPrime(long num) {
        if ((num % 2) == 0)
            return false;
        
        for (int i = 3; i * i <= num; i += 2) {
            if ((num % i) == 0)
                return false;
        }
        
        return true;
    }
    
}
