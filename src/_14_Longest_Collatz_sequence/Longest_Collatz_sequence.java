package _14_Longest_Collatz_sequence;


/**
 * https://projecteuler.net/problem=14
 * @author JenChieh
 */
public class Longest_Collatz_sequence {
    
    private final static int BELOW_NUM = 1000000;
    
    public static void main(String[] args) {
        long greatestCount = 0;
        long greatestCountNumber = 0;
        
        
        for (long num= 1; num < BELOW_NUM; ++num) {
            long count = countCollatzNumber(num);
            
            // Update the greatest count.
            if (count > greatestCount) {
                greatestCount = count;
                greatestCountNumber = num;
            }
        }
        
        System.out.println("The longest chain under " 
        + BELOW_NUM 
        + " is " 
        + greatestCountNumber 
        + " have " 
        + greatestCount);
    }
    
    /**
     * Count Collatz Number's chain by starting number.
     * @param startingNum : number to start the chain.
     * @return { int } : Return the chain count.
     */
    private static long countCollatzNumber(long startingNum) {
        return countCollatzNumber(startingNum, 0);
    }
    /**
     * Count Collatz Number's chain by starting number.
     * @param num : number to start the chain.
     * @param count : accumlate the count.
     * @return { int } : Return the chain count.
     */
    private static long countCollatzNumber(long num, long count) {
        ++count;
        
        if (num == 1)
            return count;
        
        if (isEven(num))
            return countCollatzNumber(num / 2, count);
        else 
            return countCollatzNumber((3 * num ) + 1, count);
    }
    
    /**
     * Check if number even.
     * @param num : number to check.
     * @return true : even number, false : odd number.
     */
    private static boolean isEven(long num) {
        return ((num % 2) == 0);
    }
}
