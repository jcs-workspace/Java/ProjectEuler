package _12_Highly_divisible_triangular_number;

/**
 * https://projecteuler.net/problem=12
 * @author JenChieh
 */
public class Highly_divisible_triangular_number {
    
    private final static int OVER_TARGET_DIVISOR = 500;
    
    public static void main(String[] args) {
        
        int firstTriangleNum = 0;
        
        int highestDivisorsCount = 1;
        
        for (int nth = 1; true; ++nth) {
            int triangularNum = findNthTraingular(nth);
            int divisorNum = howManyDivisors(triangularNum);
            
            if (divisorNum >= OVER_TARGET_DIVISOR) {
                firstTriangleNum = triangularNum;
                break;
            }
            
            if (highestDivisorsCount < divisorNum) {
                highestDivisorsCount = divisorNum;
            }
            
            System.out.println("Triangular number: " + triangularNum);
            System.out.println("Divisor count: " + divisorNum);
            System.out.println("Current highest divisor count: " + highestDivisorsCount);
        }
        
        System.out.println("First triangle number to have over " 
        + OVER_TARGET_DIVISOR 
        + " divisors is " 
        + firstTriangleNum);
    }
    
    /**
     * Find out how many divisor is this value.
     * @param inNum : target number to find out.
     * @return Return divisor count.
     */
    private static int howManyDivisors(int inNum) {
        int count = 0;
        
        // prime number only have 2 divisors.
        if (isPrime(inNum))
            return 2;
        
        for (int num = 1; num <= inNum; ++num) {
            if (num == (inNum / 2)) {
                ++count;
                break;
            }
            
            if ((inNum % num) == 0) 
                ++count;
        }
        
        return count;
    }
    
    /**
     * Return the n-th of triangular number.
     * 
     * @param nth : target n-th number of triangular number.
     * @return Return the n-th of triangular number.
     */
    private static int findNthTraingular(int nth) {
        return findNthTraingular(nth, 0, 0);
    }
    
    /**
     * Return the n-th of triangular number.
     * 
     * @param nth : target n-th number of triangular number.
     * @param lastNum : last number to accumilate.
     * @param inCount : counter.
     * @return Return the n-th of triangular number.
     */
    private static int findNthTraingular(int nth, int lastNum, int inCount) {
        int currentNum = lastNum + 1;
        ++inCount;
        
        if (nth == inCount)
            return currentNum;
        
        return findNthTraingular(nth, currentNum, inCount) + currentNum;
    }
    
    /**
     * Check if the number prime number.
     * 
     * @param num : number to check.
     * @return true : is prime number, false : vice versa.
     */
    private static boolean isPrime(long num) {
        if ((num % 2) == 0)
            return false;
        
        for (int i = 3; i * i <= num; i += 2) {
            if ((num % i) == 0)
                return false;
        }
        
        return true;
    }
}
