package _15_Lattice_paths;

/**
 * https://projecteuler.net/problem=15
 * @author JenChieh
 */
public class Lattice_paths {
    
    private final static int ROWS = 20;
    private final static int COLUMNS = 20;
    
    public final static void main(final String[] args) {
        
        int centerPoints = (ROWS - 1) * (COLUMNS - 1);
        
        int totalRoutes = ROWS + COLUMNS + (centerPoints * 2);
        
        System.out.println("Total routes is : " + totalRoutes);
    }
    
}
