package _6_Sum_square_difference;

/**
 * https://projecteuler.net/problem=6
 * @author JenChieh
 */
public class Sum_square_difference {
    
    private final static int START = 1;
    private final static int END = 100;
    
    public static void main(String[] args) {
        System.out.println("Find the difference between the sum of the "
                + "squares of the first one hundred natural numbers and "
                + "the square of the sum.");
        System.out.println(sqrAll(START, END) - sqrEach(START, END));
    }
    
    /**
     * sum of the squares of the first ten natural numbers is, 
     * ex: 1^2 + 2^2 + ... + 10^2 = 385
     * 
     * @param inStart : starting value.
     * @param inEnd : end value.
     * @return : total sum of the number.
     */
    private static int sqrEach(int inStart, int inEnd) {
        
        int tempSqr = inStart * inStart;
        
        if (inStart == inEnd)
            return tempSqr;
        
        ++inStart;
        return sqrEach(inStart, inEnd) + tempSqr;
    }
    
    /**
     * Square of the sum of the first ten natural numbers is, 
     * ex: (1 + 2 + ... + 10)^2 = 55^2 = 3025
     * 
     * @param inStart : starting value.
     * @param inEnd : end value.
     * @return : total sum of the number.
     */
    private static int sqrAll(int inStart, int inEnd) {
        return sqrAll(inStart, inEnd, true);
    }
    
    /**
     * Square of the sum of the first ten natural numbers is, 
     * ex: (1 + 2 + ... + 10)^2 = 55^2 = 3025
     * 
     * @param inStart : starting value.
     * @param inEnd : end value.
     * @param first : first enter the function?
     * @return : total sum of the number.
     */
    private static int sqrAll(int inStart, int inEnd, boolean first) {
        int tempSum = inStart;
        
        if (inStart == inEnd)
            return inStart;
        
        ++inStart;
        if (first)
            return (int) Math.pow(sqrAll(inStart, inEnd, false) + tempSum, 2);
        else 
            return sqrAll(inStart, inEnd, false) + tempSum;
    }
    
}
