package _1_Multiples_of_3_and_5;

/**
 * https://projecteuler.net/problem=1
 * @author JenChieh
 */
public class Multiples_of_3_and_5 {
    
    private final static int BELOW = 1000;
    private final static int[] TARGET_NUMBERS = { 3, 5 };
    
    public final static void main(final String[] args) {
        
        int sum = 0;
        
        for (int index = 0; index < TARGET_NUMBERS.length; ++index) {
            
            // is below, so minus one! 
            int numRemain = ((BELOW - 1) / TARGET_NUMBERS[index]);
            
            int currentIndexSum = 0;
            
            for (int count = 1; count <= numRemain; ++count) {
                currentIndexSum += TARGET_NUMBERS[index] * count;
            }
            
            sum += currentIndexSum;
            System.out.println("Sum of all multiples of " + TARGET_NUMBERS[index] + " below " + BELOW + " is " + currentIndexSum);
        }
        
        printSumResult(sum);
    }
    
    private static void printSumResult(int sum) {
        System.out.print("Total sum of all the multiples of ");
        
        for (int index = 0; index < TARGET_NUMBERS.length; ++index) {
            if (index != 0)
                System.out.print(" and ");
            System.out.print(TARGET_NUMBERS[index]);
        }
        
        System.out.print(" below " + BELOW + " is " + sum);
    }
    
}
