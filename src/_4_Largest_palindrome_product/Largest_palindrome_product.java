package _4_Largest_palindrome_product;

/**
 * https://projecteuler.net/problem=4
 * @author JenChieh
 */
public class Largest_palindrome_product {
    
    private final static int PALINDROME_DIGIT = 3;
    
    public final static void main(final String[] args) {
        
        int largestDigitVal = 998001;
        int largestPossiblePalindrome = 0;
        
        for (int num = largestDigitVal; 
                largestDigitVal !=0; 
                --num) {
            if (isPalindrome(num) && !isPrime(num)) {
                largestPossiblePalindrome = num;
                break;
            }
        }
        
        System.out.println(
                "Largest palindrome made from the product of two " 
        + PALINDROME_DIGIT 
        + "-digit numbers is " 
        + largestPossiblePalindrome);
    }
    
    /**
     * Check if the number is palindrome?
     * 
     * @param num : number to check.
     * @return true : is palindrome, false : vice versa.
     */
    private static boolean isPalindrome(Integer num) {
        int totalDigit = num.toString().length();
        
        int checkDigit = totalDigit;
        int fixedDigit = 0;
        
        if (isEven(totalDigit)) {
            fixedDigit = 1;
            checkDigit -= fixedDigit;
        }
        
        for (int digit = 1; digit <= checkDigit / 2 + 1; ++digit) {
            if (getUnitDigit(digit, num) != getUnitDigit(checkDigit - digit + fixedDigit + 1, num))
                return false;
        }
        
        return true;
    }
    
    /**
     * Check if number is an even number.
     * 
     * @param num : number to check is even.
     * @return true : is even number, false : odd number.
     */
    private static boolean isEven(int num) {
        return (num % 2) == 0;
    }
    
    /**
     * Get the digit number by digit index.
     * 
     * @param digit : digit in the number.
     * @param num : number to find the digit.
     * @return unit digit number in the `num'.
     */
    private static int getUnitDigit(int digit, Integer num) {
        int totalDigit = num.toString().length();
        
        if (digit > totalDigit)
            return -1;
        
        int digitCount = (int)Math.pow(10, digit);
        
        int remainder = num % digitCount;
        int po = digit - 1;
        int divider = (int)Math.pow(10, po);
        
        return remainder / divider;
    }
    
    /**
     * Check if the number prime number.
     * 
     * @param num : number to check.
     * @return true : is prime number, false : vice versa.
     */
    private static boolean isPrime(long num) {
        if ((num % 2) == 0)
            return false;
        
        for (int i = 3; i * i <= num; i += 2) {
            if ((num % i) == 0)
                return false;
        }
        
        return true;
    }
    
}
