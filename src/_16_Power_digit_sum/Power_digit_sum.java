package _16_Power_digit_sum;

/**
 * https://projecteuler.net/problem=16
 * @author JenChieh
 */
public class Power_digit_sum {
    
    public static void main(String[] args) {
        Long num = (long) Math.pow(2, 1000);
        
        System.out.println("Result of 2^1000 is " + num);
        
        long sum = 0;
        for (int digit = 0; digit < num.toString().length(); ++digit) {
            long digitNum = Long.parseLong(num.toString().substring(digit, digit + 1));
            sum += digitNum;
            System.out.print(digitNum);
        }
        
        System.out.println("\nSum of 2^100 is " + sum);
    }
}
