package Utility;


/**
 * Math utilities function put/design here...
 * @author JenChieh
 */
public class MathUtil {
    
    /**
     * Check if number is an even number.
     * 
     * @param num : number to check is even.
     * @return true : is even number, false : odd number.
     */
    public static boolean isEven(long num) {
        return (num % 2) == 0;
    }
    
    /**
     * Check if number is an even number.
     * 
     * @param num : number to check is even.
     * @return true : is even number, false : odd number.
     */
    public static boolean isEven(int num) {
        return (num % 2) == 0;
    }
    
    /**
     * Check if the number prime number.
     * 
     * @param num : number to check.
     * @return true : is prime number, false : vice versa.
     */
    public static boolean isPrime(long num) {
        if ((num % 2) == 0)
            return false;
        
        for (int i = 3; i * i <= num; i += 2) {
            if ((num % i) == 0)
                return false;
        }
        
        return true;
    }
    
    /**
     * Get the digit number by digit index.
     * 
     * @param digit : digit in the number.
     * @param num : number to find the digit.
     * @return unit digit number in the `num'.
     */
    public static int getUnitDigit(int digit, Integer num) {
        int totalDigit = num.toString().length();
        
        if (digit > totalDigit)
            return -1;
        
        int digitCount = (int)Math.pow(10, digit);
        
        int remainder = num % digitCount;
        int po = digit - 1;
        int divider = (int)Math.pow(10, po);
        
        return remainder / divider;
    }
    
    /**
     * Get the digit number by digit index.
     * 
     * @param digit : digit in the number.
     * @param num : number to find the digit.
     * @return unit digit number in the `num'.
     */
    public static long getUnitDigit(long digit, Long num) {
        int totalDigit = num.toString().length();
        
        if (digit > totalDigit)
            return -1;
        
        long digitCount = (long)Math.pow(10, digit);
        
        long remainder = num % digitCount;
        long po = digit - 1;
        long divider = (long)Math.pow(10, po);
        
        return remainder / divider;
    }
}
