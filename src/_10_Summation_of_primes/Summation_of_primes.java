package _10_Summation_of_primes;

/**
 * https://projecteuler.net/problem=10
 * @author JenChieh
 */
public class Summation_of_primes {
    
    private static final int BELOW_TARGET = 2000000;
    
    public static void main(String[] args) {
        
        int sum = 0;
        
        for (int num = 0; num <= BELOW_TARGET; ++num) {
            if (isPrime(num))
                sum += num;
        }
        
        System.out.println("Sum of all primes below " + BELOW_TARGET + " is " + sum);
    }
    
    /**
     * Check if the number prime number.
     * 
     * @param num : number to check.
     * @return true : is prime number, false : vice versa.
     */
    private static boolean isPrime(long num) {
        if ((num % 2) == 0)
            return false;
        
        for (int i = 3; i * i <= num; i += 2) {
            if ((num % i) == 0)
                return false;
        }
        
        return true;
    }
}
