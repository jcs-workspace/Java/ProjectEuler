package _9_Special_Pythagorean_triplet;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * https://projecteuler.net/problem=9
 * @author JenChieh
 */
public class Special_Pythagorean_triplet {
    
    public static void main(String[] args) {
        HashSet<ArrayList<Integer>> allPossibleCombination = findAllPossibleCombination(3, 1000);
        
        ArrayList<Integer> greatestProductCombination = new ArrayList<Integer>();
        
        int greatestProduct = 0;
        
        for (ArrayList<Integer> oneCombination : allPossibleCombination) {
            int a = oneCombination.get(0);
            int b = oneCombination.get(1);
            int c = oneCombination.get(2);
            if (!isPythagorean(a, b, c))
                continue;
            
            // NOTE(jayces): We can split this into two step, so it will 
            // be much readable.
            // record greatest product.
            if (greatestProduct < (a * b * c))
                greatestProduct = a * b * c;
            
            greatestProductCombination = oneCombination;
        }
        
        System.out.println();
        System.out.println("========================================================================");
        System.out.println("= Greatest product that match 1000 is " + greatestProduct);
        System.out.println("= The combination is -> " + greatestProductCombination);
        System.out.println("========================================================================");
    }
    
    /**
     * Check if the value is fit Pythagorean formula.
     * @return
     */
    public static boolean isPythagorean(int a, int b, int c) {
        return (Math.pow(a, 2) + Math.pow(b, 2)) == Math.pow(c, 2);
    }
    
    /**
     * Fina all the possible combination with a total number and variable count.
     * 
     * For instance: inVarCount = 3 and inTotal is 3.
     * Then the only possible combinationn is '1, 1, 1'.
     * 
     * @param inVarCount : split into how many part from total number.
     * @param inTotal : total number to be split into variable coiunt.
     * @return set of possible combination.
     */
    public static HashSet<ArrayList<Integer>> findAllPossibleCombination(int inVarCount, int inTotal) {
        HashSet<ArrayList<Integer>> possibleCombinations = new HashSet<ArrayList<Integer>>();
        
        /* 
         * Result can only be one if this is true.
         * for example total equal to 10 and variable count is 10.
         * meaning there is only one combination which is ten of 1.
         */
        if (inVarCount == inTotal) {
            ArrayList<Integer> oneCombination = new ArrayList<Integer>();
            for (int count = 0; count < inTotal; ++count)
                oneCombination.add(1);
            possibleCombinations.add(oneCombination);
            return possibleCombinations;
        }
        
        if (inVarCount > inTotal) {
            System.err.println("Variable count cannot be "
                    + "larger than total number...");
            return null;
        }
        
        int lastPartIndex = inVarCount - 1;
        int secondLastPartIndex = inVarCount - 2;
        
        for (int count = 1; true ; ++count) {
            
            ArrayList<Integer> startingCombination = new ArrayList<Integer>();
            
            /* Add each starting index combination. */
            for (int count2 = 1; count2 <= inVarCount; ++count2) {
                /* Start with '1,1,8' if total is 10. */
                if (count2 == inVarCount)
                    startingCombination.add(inTotal - ((inVarCount - 1) * count));
                else
                    startingCombination.add(count);
            }
            possibleCombinations.add(startingCombination);
            
            System.out.println("Starting: " + startingCombination);
            
            if ((startingCombination.get(0) + 1) >= startingCombination.get(lastPartIndex))
                break;
            
            // If something like'1,5,4' or '1,6,6' happens jump to next 
            // possible combination.
            if (startingCombination.get(lastPartIndex) <= startingCombination.get(secondLastPartIndex)) 
                continue;
            
            /* Start playing the combination. */
            for (int count2 = 1; true ; ++count2) {
                
                // make a copy of the starting combination.
                ArrayList<Integer> oneCombination = new ArrayList<Integer>(startingCombination);
                
                // minus count (1) to the last index
                oneCombination.set(lastPartIndex, oneCombination.get(lastPartIndex) - count2);
                
                // plus count (1) to the second last
                oneCombination.set(secondLastPartIndex, oneCombination.get(secondLastPartIndex) + count2);
                
                // compare again
                if (oneCombination.get(lastPartIndex) <= oneCombination.get(secondLastPartIndex)) 
                    break;
                
                System.out.println("One combination: " + oneCombination);
                
                // no problem we add it to the list.
                possibleCombinations.add(oneCombination);
            }
            
        }
        
        return possibleCombinations;
    }
    
}
