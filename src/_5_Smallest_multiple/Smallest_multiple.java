package _5_Smallest_multiple;

import java.util.HashSet;

/**
 * https://projecteuler.net/problem=5
 * @author JenChieh
 */
public class Smallest_multiple {
    
    private final static int START = 1;
    private final static int END = 20;
    
    // prevent duplicate value.
    private static HashSet<Integer> LIST = new HashSet<Integer>();
    
    public final static void main(final String[] args) {
        
        int evenDivide = 1;
        
        for (int num = START; num <= END; ++num) {
            addFactor(num);
        }
        
        for (Integer val : LIST) {
            evenDivide *= val;
            System.out.println(val);
        }
        
        System.out.println(
                "Smallest positive number that is evenly divisible "
                + "by all of the numbers from " + START + " to " + END 
                + " is " + evenDivide);
    }

    /**
     * Add all possible factor to the `set'.
     * @param inNum : number we trying find out all the factors.
     */
    private static void addFactor(int inNum) {
        boolean didAdd = false;
        
        for (int num = 1; num <= inNum; ++num) {
            if ((inNum % num) == 0) {
                LIST.add(num);
                
                if (num != 1 && num != inNum)
                    didAdd = true;
            }
            
            // check if two factor the same value.
            // ex: 3 * 3 = 9, in this case we just want the 9 not 3.
            if ((inNum / num) == num) {
                // ex: remove the 3.
                LIST.remove(num);
                didAdd = false;
            }
        }
        
        if (didAdd) {
            LIST.remove(inNum);
        }
        
    }
    
}
