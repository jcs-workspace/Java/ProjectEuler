package _3_Largest_prime_factor;


/**
 * https://projecteuler.net/problem=3
 * @author JenChieh
 */
public class Largest_prime_factor {
    
    private final static long TARGET_PRIME_FACTOR = 600851475143L;
    //private final static long TARGET_PRIME_FACTOR = 13195L;
    
    public final static void main(final String[] args) {
        // whatever times 1 get the same result, so start at 2.
        long largestPrimeFactor = 2;
        
        long checkReachTarget = 1;
        
        for (long num = 1; num <= TARGET_PRIME_FACTOR / 2; num += 2) {
            if (isPrime(num)) {
                if ((TARGET_PRIME_FACTOR % num) == 0) {
                    // record down the largest prime number.
                    largestPrimeFactor = num;
                    System.out.println(num);
                    
                    /* This speed up the whole process. */
                    checkReachTarget *= largestPrimeFactor;
                    if (checkReachTarget == TARGET_PRIME_FACTOR)
                        break;
                }
            }
        }
        
        System.out.println("Largest prime factor is " + largestPrimeFactor);
    }
    
    /**
     * Check if the number prime number.
     * 
     * @param num : number to check.
     * @return true : is prime number, false : vice versa.
     */
    private static boolean isPrime(long num) {
        if ((num % 2) == 0)
            return false;
        
        for (int i = 3; i * i <= num; i += 2) {
            if ((num % i) == 0)
                return false;
        }
        
        return true;
    }
    
}
